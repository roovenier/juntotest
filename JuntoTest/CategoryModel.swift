//
//  CategoryModel.swift
//  JuntoTest
//
//  Created by Александр Ощепков on 22.08.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit

class CategoryModel: NSObject {
    var id: Int
    var color: String
    var name: String
    var slug: String
    
    init(object: [String: Any]) {
        self.id = object["id"] as! Int
        self.color = object["color"] as! String
        self.name = object["name"] as! String
        self.slug = object["slug"] as! String
        
        super.init()
    }
}
