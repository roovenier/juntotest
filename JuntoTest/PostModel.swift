//
//  PostModel.swift
//  JuntoTest
//
//  Created by Александр Ощепков on 23.08.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit

class PostModel: NSObject {
    var id: Int
    var name: String
    var tagline: String
    var upvotes: Int
    var thumbnailURL: URL?
    var postLink: URL
    var screenshotURL: URL?
    
    init(object: [String: Any]) {
        self.id = object["id"] as! Int
        self.name = object["name"] as! String
        self.tagline = object["tagline"] as! String
        self.upvotes = object["votes_count"] as! Int
        
        if let thumbnailObject = object["thumbnail"] as? [String: Any] {
            self.thumbnailURL = URL.init(string: thumbnailObject["image_url"] as! String)!
        }
        
        self.postLink = URL.init(string: object["redirect_url"] as! String)!
        
        if let screenshotObject = object["screenshot_url"] as? [String: Any] {
            self.screenshotURL = URL.init(string: screenshotObject["300px"] as! String)!
        }
        
        super.init()
    }
}
