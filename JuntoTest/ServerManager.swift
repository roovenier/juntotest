//
//  ServerManager.swift
//  JuntoTest
//
//  Created by Александр Ощепков on 21.08.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit
import Alamofire
import PromiseKit
import AlamofireImage
import SwiftGifOrigin

final class ServerManager {

    private init() {}
    
    static let instance = ServerManager()
    
    let basicURL = "https://api.producthunt.com/v1"
    let access_token = "591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff"
    
    func getCategories() -> Promise<[String]> {
        let url = URL.init(string: basicURL + "/categories")
        let params = ["access_token": access_token]
        
        return Promise { fulfill, reject in
            Alamofire.request(url!, method: .get, parameters: params, headers: nil).responseJSON { response in
                if let result = response.result.value {
                    var categoriesArray = [String]()
            
                    let jsonResult = result as! [String: Array<[String: Any]>]
                    for item in jsonResult["categories"]! {
                        categoriesArray.append(item["name"] as! String)
                    }
                            
                    fulfill(categoriesArray)
                }
            }
        }
    }
    
    func getPosts(category: String) -> Promise<[PostModel]> {
        return Promise { fulfill, reject in
            let url = URL.init(string: basicURL + "/categories/" + category.lowercased() + "/posts")
            let params = ["access_token": access_token]
            
            Alamofire.request(url!, method: .get, parameters: params, headers: nil).responseJSON { response in
                if let result = response.result.value {
                    var postsArray = [PostModel]()
                    
                    let jsonResult = result as! [String: Array<[String: Any]>]
                    for item in jsonResult["posts"]! {
                        let post = PostModel.init(object: item)
                        postsArray.append(post)
                    }
                    
                    fulfill(postsArray)
                }
            }
        }
    }
    
    func getImageFromLink(imageURL: URL, completionHandler: @escaping (Image) -> Void) {
        var urlComponents = URLComponents(url: imageURL, resolvingAgainstBaseURL: false)
        let queryItems = [URLQueryItem(name: "auto", value: "format"), URLQueryItem(name: "fit", value: "crop"), URLQueryItem(name: "w", value: "100"), URLQueryItem(name: "h", value: "100")]
        urlComponents?.queryItems = queryItems
        let imageFullURL = urlComponents?.string!
        
        let gifImage = UIImage.gif(url: imageFullURL!)
        
        if gifImage != nil {
            completionHandler(gifImage!)
        } else {
            print("Error on loading gif")
        }
    }
    
    func getScreenshotFromLink(imageURL: URL, completionHandler: @escaping (Image) -> Void) {
        Alamofire.request(imageURL, method: .get, parameters: nil, headers: nil).responseImage { response in
            completionHandler(response.result.value!)
        }
    }
}
