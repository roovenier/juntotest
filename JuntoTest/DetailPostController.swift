//
//  DetailPostController.swift
//  JuntoTest
//
//  Created by Александр Ощепков on 27.08.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit

class DetailPostController: UIViewController {
    
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postDescription: UILabel!
    @IBOutlet weak var postUpvotes: UILabel!
    @IBOutlet weak var postPoster: UIImageView!
    
    var post: PostModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = post?.name
        
        postTitle.text = post?.name
        postDescription.text = post?.tagline
        postUpvotes.text = String(post!.upvotes)
        
        ServerManager.instance.getScreenshotFromLink(imageURL: post!.screenshotURL!) { screenshot in
            self.postPoster.image = screenshot
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getItAction(_ sender: UIButton) {
        UIApplication.shared.open(post!.postLink, options: [:], completionHandler: nil)
    }
}
