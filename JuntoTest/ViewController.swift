//
//  ViewController.swift
//  JuntoTest
//
//  Created by Александр Ощепков on 20.08.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu
import PromiseKit
import Alamofire

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var categoriesList = [String]()
    var postsList = [PostModel]()
    var refreshControl: UIRefreshControl!
    var currentTitleIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        
        categoriesRequest()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(pullRefresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    func pullRefresh() {
        postsRequest(categoryName: categoriesList[currentTitleIndex])
    }

    func categoriesRequest() {
        //ServerManager.instance.getCategories()
        
        _ = firstly {
            ServerManager.instance.getCategories()
        }.then { categories -> Promise<[PostModel]> in
            self.categoriesList = categories
            self.setupNavigation()
            return ServerManager.instance.getPosts(category: categories[0])
        }.then { posts -> Void in
            self.postsList = posts
            self.tableView.reloadData()
        }
    }
    
    func postsRequest(categoryName: String) {
        _ = firstly {
            ServerManager.instance.getPosts(category: categoryName)
        }.then { posts -> Void in
            self.postsList = posts
            self.tableView.reloadData()
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func setupNavigation() {
        if self.categoriesList.count > 0 {
            let menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: categoriesList[0], items: categoriesList)
            
            self.navigationItem.titleView = menuView
            
            menuView.cellSelectionColor = UIColor.lightGray.withAlphaComponent(0.25)
            menuView.arrowTintColor = UIColor.black
            menuView.checkMarkImage = UIImage.init(named: "checkmark_icon")
            menuView.cellSeparatorColor = UIColor.lightGray
            
            menuView.didSelectItemAtIndexHandler = {[weak self] (indexPath: Int) -> () in
                self?.currentTitleIndex = indexPath
                self!.postsRequest(categoryName: self!.categoriesList[indexPath])
            }
        }
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = postsList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell") as! PostCell
        
        cell.thumbnailView.image = nil
        
        cell.titleLabel.text = post.name
        cell.descriptionLabel.text = post.tagline
        cell.upvotesLabel.text = String(post.upvotes)
        
        let queue = OperationQueue()
        queue.qualityOfService = .userInitiated
        queue.addOperation {
            ServerManager.instance.getImageFromLink(imageURL: post.thumbnailURL!) { thumbnail in
                OperationQueue.main.addOperation {
                    cell.thumbnailView.image = thumbnail
                }
            }
        }
        
        return cell
    }
    
    // MAARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailPostController") as! DetailPostController
        vc.post = postsList[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
    }
}

